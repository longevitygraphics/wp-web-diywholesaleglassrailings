<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
	<?php do_action( 'wp_content_bottom' ); ?>
	</div>
	
	<?php do_action( 'wp_body_end' ); ?>
	<?php $lg_option_footer_site_legal = get_option( 'lg_option_footer_site_legal' ); ?>

	<footer id="site-footer">
		
		<div>
			<?php if ( ! $lg_option_footer_site_legal || $lg_option_footer_site_legal == 'enable' ) : ?>
				<div id="site-legal" class="py-3 px-3 container">
					<div class="d-xl-flex justify-content-center justify-content-md-between align-items-center flex-wrap">
						<div class="footer-legal-left mb-1 mb-xl-0"><a href="/privacy-policy">Privacy Policy</a></div>
						<div class="d-md-flex justify-content-center justify-content-xl-end">
							<div class="site-info"><?php get_template_part( '/templates/template-parts/footer/site-info' ); ?></div>
								<span class="d-none d-md-block mx-1"> | </span>
							<div class="site-longevity"> <?php get_template_part( '/templates/template-parts/footer/site-footer-longevity' ); ?> </div>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>

	</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>

<?php do_action( 'document_end' ); ?>
