<div class="main-navigation container">
	<nav class="navbar navbar-expand-xl">
		<button class="navbar-toggler hamburger hamburger--squeeze js-hamburger" type="button" data-toggle="offcanvas">
			<div class="hamburger-box">
				<div class="hamburger-inner"></div>
			</div>
		</button>

		<!-- Main Menu  -->
		<div class="navbar-collapse offcanvas-collapse" id="main-navbar" >
			<?php

			$mainMenu = array(
				// 'menu'              => 'menu-1',
				'theme_location' => 'top-nav',
				'depth'          => 2,
				'container'      => false,
				/*'container' => 'div',
				'container_class' => 'navbar-collapse offcanvas-collapse bg-primary',
				'container_id' => 'main-navbar',*/
				'menu_class'     => 'navbar-nav mr-auto',
				'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
				'walker'         => new WP_Bootstrap_Navwalker()
			);
			wp_nav_menu( $mainMenu );

			?>
		</div>
		<a class="header-mobile-phone d-md-none" href="<?php echo do_shortcode("[lg-phone-main]")?>">CALL <?php echo do_shortcode("[lg-phone-main]")?></a>
	</nav>
</div>
