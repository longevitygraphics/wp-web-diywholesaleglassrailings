<?php
/**
 * The header for our theme
 *
 */

?>

<?php do_action('document_start'); ?>

<!doctype html>
<html <?php language_attributes(); ?> <?php do_action('html_class'); ?>>
<head>
  <!--[if lt IE 9]>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
  <![endif]-->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
  <?php do_action('wp_header'); ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

  <?php do_action('wp_body_start'); ?>

  <div id="page" class="site">

  	<header id="site-header">

      <div class="header-top">
        <div class="container d-flex justify-content-between">
            <div class="site-logos d-flex">
                <a class="diy-logo" href="<?php echo get_site_url() ?>"><?php echo site_logo_alt(); ?></a>
                <a class="main-logo" href="<?php echo get_site_url() ?>"><?php echo site_logo(); ?></a>
            </div>
            <a class="header-phone align-self-center d-none d-md-inline-block" href="tel: <?php echo do_shortcode("[lg-phone-main]")?>">CALL <?php echo do_shortcode("[lg-phone-main]")?></a>
        </div>  
    </div>

      <div class="header-main">
          <?php get_template_part("/templates/template-parts/header/main-nav"); ?>
      </div>

    </header><!-- #masthead -->

    <div id="site-content" role="main">
      <?php do_action('wp_content_top'); ?>
