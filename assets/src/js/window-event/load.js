// Windows Load Handler
// ​(function ($) {
//     $(window).on("load", function () {});
// })(jQuery);
// ​
let cls = 0;
new PerformanceObserver((entryList) => {
    for (const entry of entryList.getEntries()) {
        if (!entry.hadRecentInput) {
            cls += entry.value;
            console.log("Current CLS value:", cls, entry);
            // debugger;
            if (cls >= 0.1) {
                console.log("CLS FAILING");
            }
        }
    }
}).observe({ type: "layout-shift", buffered: true });